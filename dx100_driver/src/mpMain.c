/** \brief Motoman Driver modification for the purpose of CloPeMa project
 *  \param Controller DX100
 */

#define DX100

#include <motoPlus.h>
#include "ParameterExtraction.h"
#include "CtrlGroup.h"
#include "SimpleMessage.h"
#include "Controller.h"
#include "StateServer.h"
#include "MotionServer.h"


/** \brief Initialize ROS Driver Task */
void RosInitTask();

/** \brief Test Parameter Extraction Class */
void TestParamExtractionTask();

int RosInitTaskID, TestParamExtractionTaskID;

void mpUsrRoot(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7, int arg8, int arg9, int arg10) {
#ifdef DX100
    mpTaskDelay(10000);  // 10 sec. delay to enable DX100 system to complete initialization
#endif
    //Creates and starts a new task in a seperate thread of execution.
    RosInitTaskID = mpCreateTask(MP_PRI_TIME_NORMAL, MP_STACK_SIZE, (FUNCPTR)RosInitTask,
                                 arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);

    mpExitUsrRoot; //Ends the initialization task.
}

void TestParamExtractionTask(Controller *controller) {
   /* FOREVER {
        void* stopWatchId = NULL;
        int i = 0;
        UINT16 value;
        STATUS st;
        mpTaskDelay(10000);
        printf("starting %d \n\r", i);
	stopWatchId = mpStopWatchCreate(1);
	mpStopWatchStart(stopWatchId);
        for(i = 0; i < 1000; i++) {
            mpClkAnnounce(MP_INTERPOLATION_CLK);
        }
        mpStopWatchStop(stopWatchId);
	printf("process time = %f \r\n" , mpStopWatchGetTime(stopWatchId));
        printf("received ALL, waiting 10sec\r\n");
        st = GP_getInterpolationPeriod(&value);
        printf("[PE] Interpolation Period: %d %hu\n", st, value);
    }
    while(1) {
        UINT16 value;
        STATUS st;
        int groupNo;
        value = 0;
        printf("[PE] Num of groups: %d\n", GP_getNumberOfGroups());
        printf("[PE] Num of axes 1: %d\n", GP_getNumberOfAxes(0));
        printf("[PE] Num of axes 2: %d\n", GP_getNumberOfAxes(1));
        printf("[PE] Num of axes 3: %d\n", GP_getNumberOfAxes(2));
        st = GP_getInterpolationPeriod(&value);
        printf("[PE] Interpolation Period: %d %hu\n", st, value);

        printf("Controller, num_robot: %d\n", controller->numRobot);

        for(groupNo = 0; groupNo < controller->numGroup; groupNo++) {
            int i;
            CtrlGroup *ctrlGroup =  controller->ctrlGroups[groupNo];
            for(i = 0; i < MAX_PULSE_AXES; i++) {
                printf("Max speed for group %d, axis %d: %f\n", groupNo, i, ctrlGroup->maxSpeedRad[i]);
            }
        }
        mpTaskDelay(10000);
    }*/

}


void RosInitTask() {
    Controller ros_controller;
//     int i;
//     for(i = 0; i < 20; i++) {
//         mpTaskDelay(1000);
//         printf("RosInitTask started: %d / %d \n", i, 20);
//     }

    if(!Ros_Controller_Init(&ros_controller)) {
        mpDeleteSelf;
        return;
    }

    ros_controller.tidConnectionSrv = mpCreateTask(MP_PRI_TIME_NORMAL, MP_STACK_SIZE,
                                      (FUNCPTR)Ros_Controller_ConnectionServer_Start,
                                      (int)&ros_controller, 0, 0, 0, 0, 0, 0, 0, 0, 0);

#ifdef DX100
// DX100 need to execute a SKILLSEND command prior to the WAIT in order for the
// incremental motion function to work.  These tasks monitor for those commands
// This supports a maximum of two robots which should be assigned to slave id
// MP_SL_ID1 and MP_SL_ID2 respectively.

// first robot
    ros_controller.RosListenForSkillID[0] = mpCreateTask(MP_PRI_TIME_NORMAL, MP_STACK_SIZE,
                                            (FUNCPTR)Ros_Controller_ListenForSkill,
                                            (int)&ros_controller, MP_SL_ID1, 0, 0, 0, 0, 0, 0, 0, 0);
// if second robot
    if(ros_controller.numRobot > 1) {
        ros_controller.RosListenForSkillID[1] = mpCreateTask(MP_PRI_TIME_NORMAL, MP_STACK_SIZE,
                                                (FUNCPTR)Ros_Controller_ListenForSkill,
                                                (int)&ros_controller, MP_SL_ID2, 0, 0, 0, 0, 0, 0, 0, 0);
    } else {
        ros_controller.RosListenForSkillID[1] = INVALID_TASK;
    }
#endif

//     TestParamExtractionTaskID = mpCreateTask(MP_PRI_TIME_NORMAL, MP_STACK_SIZE, (FUNCPTR)TestParamExtractionTask,
//                                 (int)&ros_controller, 0, 0, 0, 0, 0, 0, 0, 0, 0);
// start loop to monitor controller state
    FOREVER {
        // Check controller status
        Ros_Controller_StatusUpdate(&ros_controller);

        mpTaskDelay(CONTROLLER_STATUS_UPDATE_PERIOD);
    }
}



