#!/bin/bash

cd src
mkdir back
mv *.c back/
cd back
find . -name "*.c" -exec iconv -c -f utf-8 -t cp1252 {} -o ../{} \;
cd ../..

cd include
mkdir back
mv *.h back/
cd back
find . -name "*.h" -exec iconv -c -f utf-8 -t cp1252 {} -o ../{} \;
cd ../..
