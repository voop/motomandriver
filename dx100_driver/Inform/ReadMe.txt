Update INIT_ROS.JBI to start jobs for groups you are interested in.
For CloPeMa robot it means ROS_R1S1 and ROS_R2 which requires to manually create group combination R1+S1 with S1 as master.
